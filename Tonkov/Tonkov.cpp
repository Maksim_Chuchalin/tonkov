﻿// Tonkov.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

#include <iostream>
#include <fstream> 
#include <math.h>
#include <locale>
#include <iomanip>


using namespace std;

double fi(double x)
{
	if (x >= 0.2 && x <= 0.8)
		return 1;
	else
	    return 0;
}
double psi(double t)
{
	return 0;//+1;
}
double func(double x, double t)
{
	return 0;
}

int main()
{
	setlocale(LC_ALL, "RUS");
	double a;  // скорость потока
	double x[120]; // Значения x
	double t[70];  // Значения y
	double u[80][110];  // таблица узлов
	int J = 50;
	int I = 100;
	double h;  // шаг сетки по h
	double tau; // шаг сетки по t
	double lmd;


	cout << "Решение уравнения переноса:" << endl;
	cout << "  dU/dt  +  a*(dU/dx)  =   F(x,t)  " << endl;

	cout << "F(x,t):  F(x,t)=x+t+1 " << endl;
	cout << "Начальное условие:  U(x,0)=fi(x)=x+3 " << endl;
	cout << "Граничное условие:  U(0,t)=psi(t)=t+1 " << endl;
	cout << "Ограниченная область G:  0<=x<=1  и  0<=t<=0.5 " << endl;

	cout << "Введите а - скорость переноса, a > 0:  "; cin >> a;
	h = 1.0 / I;
	tau = 0.5 / J;
	lmd = (a*tau) / h; // из формулы (5)
	x[0] = 0;
	t[0] = 0;

	//формулы (6)
	for (int i = 0;i <= I;i++)
	{
		x[i + 1] = x[i] + h;
		u[0][i] = fi(x[i]);
		//  cout<<x[i]<<endl;
		// cout<<u[0][i]<<endl;
	}

	// формулы (7, 5)
	for (int j = 0;j <= J;j++)
	{
		t[j + 1] = t[j] + tau;
		u[j + 1][0] = psi(t[j + 1]);
		cout << "T array:" << endl;
		cout << t[j] << endl;
		// cout<<u[j+1][0]<<endl;
		for (int i = 1;i <= I;i++)
		{
			u[j + 1][i] = lmd*u[j][i - 1] + (1 - lmd)*u[j][i]; //+ tau*func(x[i], t[j]);
		}

		u[j + 1][0] = u[j + 1][I];
	}
	// X = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1];
	//Y21 = [3,   3.1,   3.2,   3.3,   3.4,   3.5,   3.6,   3.7,   3.8,   3.9,     4]
	//pylab.plot(X, Y21)
	//pylab.show()
	int Y_counter = 0;

	ofstream fout;
	fout.open("cppstudio.txt");
	// вывод таблицы узлов
	for (int i = 0;i <= J;i++)
	{
		fout << "Y" << Y_counter++ << " = [";
		for (int j = 0;j <= I;j++)
		{
			//cout << setw(10) << u[i][j] << setw(10);
			fout << setw(10) << u[i][j] << setw(10) << ",";
		}

		fout << " 0 ]";
		fout << endl;
	}


	double inc = 0;
	fout << "X = [ ";
	for (int i = 0;i <= I;i++)
	{
		fout << inc << ", ";
		inc += 0.1;
	}

	fout << " 0]";
	fout << endl;

	for (int i = 0;i <= J;i++)
	{
		fout << "pylab.plot(X, Y" << i << ")" << endl;
	}
	
	fout << endl << "pylab.show()";	

	fout.close();
	system("pause");
	return 0;
}
